# Configure Kafka:


1. If you download the whole project, including kafka_2.12-2.7.0 folder, the first step is not needed.  
   If you recreate the project from scratch, download the BINARIES from: 

#       https://www.apache.org/dyn/closer.cgi?path=/kafka/2.7.0/kafka_2.12-2.7.0.tgz


2. Start a new Shell session and start Zookeeper instance on port 2181 (default)

- Zookeeper is responsible to coordinate the Kafka brokers inside your cluster
- Make sure you don't have any spaces in any of the directory names that form the
 path of the kafka location (otherwise you might get the following error: 
 Classpath is empty. Please build the project first e.g. by 
 running './gradlew jar -PscalaVersion=2.11.12')
 https://www.titanwolf.org/Network/q/c8c0ea05-e729-4c6d-9f5d-05f38de6227f/y

#        cd alert_notifier/kafka_2.12-2.7.0
#       bin/zookeeper-server-start.sh config/zookeeper.properties


3. Open a new terminal and run Kafka broker on localhost:9092 (default) 

#        cd alert_notifier/kafka_2.12-2.7.0/bin/
#       ./kafka-server-start.sh ../config/server.properties

### topic name can be configured from KafkaConfigurationConstants.java

4. Run RegistrationServer app and access http://localhost:1000, to see the registered applications, after
starting AlertConsumerServer app and WebServer app

5. Run AlertConsumerServer app

6. http://localhost:1002/deleteTopic > this will assure that the topic is re-created from scratch 
(old messages will be deleted in order to have a clean new topic)

7. http://localhost:1002/producer/   > to start the Producer (check the console logs)

6. http://localhost:1002/producer/messages   > to see the messages

7. http://localhost:1004/consumer/   > to start the Consumer

8. http://localhost:1004/consumer/messages  > to see the messages (refresh from time to time to see new messages)

9. http://localhost:1006/ > TBD : list of alerts that has to be sent

10. http://localhost:1008/logs > TBD : see aggregated logs from all microservices
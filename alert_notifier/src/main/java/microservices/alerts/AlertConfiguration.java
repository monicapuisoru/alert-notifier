package microservices.alerts;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

import javax.sql.DataSource;
import java.util.logging.Logger;

@Configuration
@ComponentScan
@EntityScan("microservices.alerts")
@EnableJpaRepositories("microservices.alerts")
@PropertySource("classpath:db-config.properties")
public class AlertConfiguration {

    protected Logger logger;

    public AlertConfiguration() {

        logger = Logger.getLogger(getClass().getName());
    }

    @Bean
    public DataSource dataSource() {
        logger.info("dataSource() invoked");


        DataSource dataSource = (new EmbeddedDatabaseBuilder()).addScript("classpath:testDB/schema.sql")
                .addScript("classpath:testDB/data.sql").build();

        logger.info("dataSource = " + dataSource);

        return dataSource;
    }
}

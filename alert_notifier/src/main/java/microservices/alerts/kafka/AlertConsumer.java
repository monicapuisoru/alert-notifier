package microservices.alerts.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class AlertConsumer {


    private final static String BOOTSTRAP_SERVER = "localhost:9092";
    public static KafkaConsumer<String, String> kafkaConsumer;
    public static HashMap<String, String> consumedMessages = new HashMap<>();
    private final Logger log = LogManager.getLogger(AlertConsumer.class.getName());
    private final AtomicBoolean closed = new AtomicBoolean(false);
    private ExecutorService executorService = Executors.newCachedThreadPool();

    @PostConstruct
    public void consumeMessages() {
        log.info("Kafka consumer starting...");
        System.setProperty("server.port", "8091");
        generateKafkaConsumer();

        kafkaConsumer.subscribe(Collections.singletonList(KafkaConfigurationConstants.TOPIC));

        log.info("Kafka consumer started");

        executorService.execute(() -> {
            try {
               while (!closed.get()) {
                    ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, String> record : records) {
                        log.info("Consumed from topic {} : message.value {}", KafkaConfigurationConstants.TOPIC, record.value());
                        consumedMessages.put(record.key(), record.value());
                    }
                }
                kafkaConsumer.commitSync();
            } catch (WakeupException e) {
                if (!closed.get()) throw e;
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            } finally {
                log.info("Kafka consumer has been closed..");
                kafkaConsumer.close();
            }
        });
    }


    private void generateKafkaConsumer() {
        Properties props = new Properties();
        props.setProperty("bootstrap.servers", BOOTSTRAP_SERVER);
        props.setProperty("group.id", "mykafkagroup");
        props.setProperty("enable.auto.commit", "true");
        props.setProperty("auto.commit.interval.ms", "1000");
        props.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        kafkaConsumer = new KafkaConsumer<>(props);
    }


    public KafkaConsumer<String, String> getKafkaConsumer() {
        return kafkaConsumer;
    }


    public void shutdown() {
        log.info("Shutdown Kafka consumer");
        closed.set(true);
        kafkaConsumer.wakeup();
    }

}


package microservices.alerts.kafka;

public class KafkaConfigurationConstants {

    public static final String TOPIC = "topic9";
    public final static String BOOTSTRAP_SERVER ="localhost:9092";

    public final static int NO_OF_PARTITIONS = 1;
    public final static short REPLICATION_FACTOR = 1;

}

package microservices.alerts.kafka;

import org.apache.kafka.clients.admin.Admin;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaFuture;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;
@Component
public class AlertProducer {

    public static HashMap<String, String> sentMessages = new HashMap<>();
    private static KafkaProducer<String, String> producer;
    private final Logger log = LogManager.getLogger(AlertProducer.class.getName());

    @PostConstruct
    public void initialize() {
        log.info("Kafka producer initializing...");
        log.info("Creating new topic ("
                + KafkaConfigurationConstants.TOPIC
                +": no_of_partitions = "
                +KafkaConfigurationConstants.NO_OF_PARTITIONS
                +", replication_factor = "
                +KafkaConfigurationConstants.REPLICATION_FACTOR);
      //  createNewTopic();

        createProducer();
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
        log.info("Kafka producer initialized");
    }

    public void createNewTopic() throws InterruptedException, ExecutionException {
        Admin admin = Admin.create(getProperties()) ;
        NewTopic newTopic = new NewTopic(
                KafkaConfigurationConstants.TOPIC,
                KafkaConfigurationConstants.NO_OF_PARTITIONS,
                KafkaConfigurationConstants.REPLICATION_FACTOR);

        CreateTopicsResult result = admin.createTopics(Collections.singleton(newTopic));
        KafkaFuture<Void> future = result.values().get(KafkaConfigurationConstants.TOPIC);
        future.get();
    }


    public void createProducer() {
        Properties props = getProperties();
        producer = new KafkaProducer<>(props);
    }


    public Properties getProperties() {
        Properties props = new Properties();
        props.put("bootstrap.servers", KafkaConfigurationConstants.BOOTSTRAP_SERVER);
        props.put("acks", "all");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return props;
    }

    public void sendAlerts() {
        try {

               while(true) {
                   int randomNumber = ThreadLocalRandom.current().nextInt(0, 10);
                    // Every 3 seconds send a message
                    try {
                        Thread.sleep(3000);

                    } catch (InterruptedException e) {
                        log.error("Could not send alert", e);
                    }

                    String key = generateUUID();
                    String value = "alertId=" + randomNumber
                            +", coinId=" + randomNumber
                            +", userId=" + randomNumber
                            +", marketId=" + randomNumber
                            +", currentAmount=" + randomNumber
                            +", threshold=" + randomNumber;
                    producer.send(new ProducerRecord<>(KafkaConfigurationConstants.TOPIC, key, value));
                    sentMessages.put(key, value);
                    log.info("Sent: key = "+key+", value = " + value);
                }
            } finally {
                producer.close();
            }
    }


    private String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }


    public void startProducer(){
        AlertProducer alertProducer = new AlertProducer();
        alertProducer.initialize();

    }

    public void deleteTopic(String topicName) {
        Admin admin = Admin.create(getProperties()) ;
        admin.deleteTopics(Arrays.asList(topicName));
        log.info("Topic '{}' deleted.", topicName);
    }

    @PreDestroy
    public void shutdown() {
        log.info("Shutdown Kafka producer");
        producer.close();
    }

}


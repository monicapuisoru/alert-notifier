package microservices.alerts.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name="ALERTS")
public class Alert implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    protected Long alertId;
    protected Long userId;
    protected Long marketId;
    protected Long coinId;
    protected BigDecimal currentAmount;
    protected BigDecimal threshold;
    public Alert() {
        this.alertId = Long.valueOf(9999);
        this.userId = Long.valueOf(9999);
        this.marketId = Long.valueOf(9999);
        this.coinId = Long.valueOf(9999);
        this.currentAmount = new BigDecimal(9999.00);
        this.threshold = new BigDecimal(0.9999);
    }

    public Alert(Long userId, Long marketId, Long coinId, BigDecimal threshold, BigDecimal currentAmount) {
        this.userId = userId;
        this.marketId = marketId;
        this.coinId = coinId;
        this.threshold = threshold;
        this.currentAmount = currentAmount;
    }

    public Long getAlertId() {
        return alertId;
    }

    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getMarketId() {
        return marketId;
    }

    public void setMarketId(Long marketId) {
        this.marketId = marketId;
    }

    public Long getCoinId() {
        return coinId;
    }

    public void setCoinId(Long coinId) {
        this.coinId = coinId;
    }

    public BigDecimal getThreshold() {
        return threshold;
    }

    public void setThreshold(BigDecimal threshold) {
        this.threshold = threshold;
    }

    public BigDecimal getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(BigDecimal currentAmount) {
        this.currentAmount = currentAmount;
    }

    @Override
    public String toString(){
        return "Alert id: " + alertId
    + ", userId: " + userId
    + ", marketId: " + marketId
    + ", coinId: " + coinId
    + ", currentAmount: " + currentAmount
    + ", threshold: " + threshold;
    }

}

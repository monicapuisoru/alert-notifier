package microservices.alerts.controllers;

import microservices.alerts.alertRepository.AlertRepository;
import microservices.alerts.model.Alert;
import microservices.exceptions.AlertNotFoundByAlertIdException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class AlertProcessorController {
    protected Logger logger = Logger.getLogger(AlertProcessorController.class
            .getName());
    protected AlertRepository alertRepository;

    @Autowired
    public AlertProcessorController(AlertRepository alertRepository) {
        this.alertRepository = alertRepository;
    }

    @RequestMapping("/alerts/{alertId}")
    public List<Alert> byAlertId(@PathVariable("alertId") String alertId) {
        logger.info("alert-processor-server byAlertId() invoked for alertId = " + alertId);

        List<Alert>  alerts = alertRepository.findByAlertId(Long.valueOf(alertId));
        logger.info("alert-processor-server byAlertId() - found alert with alertId = " + alertId);

        if (alerts == null)
            throw new AlertNotFoundByAlertIdException(alertId);
        else {
            return alerts;
        }
    }

    @RequestMapping("/count")
    public int countAlerts() {
        logger.info("alert-processor-server countAlerts() invoked: ");
        int counter = alertRepository.countAlerts();

        if (counter < 1 )
            logger.info("alert-processor-server countAlerts() No alert found!");
        else
            logger.info("alert-processor-server countAlerts() Found " + counter + " alerts");

        return counter;
    }


    @RequestMapping("/alerts")
    public List<Alert> findAll() {

        logger.info("alert-processor-server findAll() invoked:" );
        List<Alert>  alerts = alertRepository.findAll();

        if (alerts == null)
            logger.info("alert-processor-server findAll(): no alerts found");
        else
            logger.info("alert-processor-server findAll(): Alerts found: "+alerts.size());

        return alerts;
    }

}

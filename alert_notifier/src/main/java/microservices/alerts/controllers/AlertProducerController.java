package microservices.alerts.controllers;

import microservices.alerts.alertRepository.AlertRepository;
import microservices.alerts.kafka.AlertProducer;
import microservices.alerts.kafka.KafkaConfigurationConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.logging.Logger;

@RestController
public class AlertProducerController {
    protected Logger logger = Logger.getLogger(AlertProducerController.class
            .getName());
    protected AlertRepository alertRepository;

    @Autowired
    public AlertProducerController(AlertRepository alertRepository) {
        this.alertRepository = alertRepository;
    }

    @RequestMapping("/deleteTopic")
    public String deleteTopic() {
        AlertProducer alertProducer = new AlertProducer();
        alertProducer.deleteTopic(KafkaConfigurationConstants.TOPIC);
        return "Topic " + KafkaConfigurationConstants.TOPIC +" has been deleted";
    }

    @GetMapping("/producer")
    @ResponseBody
    public void startProducer() {
        AlertProducer alertProducer = new AlertProducer();
        alertProducer.startProducer();
        alertProducer.sendAlerts();
    }

    @GetMapping("/producer/messages")
    @ResponseBody
    public String getProducedMessages() {
        String result = "";
        for (Map.Entry<String, String> e : AlertProducer.sentMessages.entrySet()) {
            String key = e.getKey();
            String value = e.getValue();
            result += "key:      "+key+",     value:     "+value+"<br>";
        }
        return result + ("<br><br><br>" + AlertProducer.sentMessages.size()
                + " messages have been produced");
    }
}

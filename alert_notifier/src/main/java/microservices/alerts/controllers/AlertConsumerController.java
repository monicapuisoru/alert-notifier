package microservices.alerts.controllers;

import microservices.alerts.alertRepository.AlertRepository;
import microservices.alerts.kafka.AlertConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.logging.Logger;

@RestController
public class AlertConsumerController {
    protected Logger logger = Logger.getLogger(AlertConsumerController.class
            .getName());
    protected AlertRepository alertRepository;

    @Autowired
    public AlertConsumerController(AlertRepository alertRepository) {
        this.alertRepository = alertRepository;
    }

    @RequestMapping(path = "/consumer", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String consumeMessages() {
        AlertConsumer alertConsumer = new AlertConsumer();
        alertConsumer.consumeMessages();
        return "Consumer has started";
    }

    @RequestMapping(path = "/consumer/messages", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String retrieveMessages() {
        String result = "";
        for (Map.Entry<String, String> e : AlertConsumer.consumedMessages.entrySet()) {
            String key = e.getKey();
            String value = e.getValue();
            result += "key = "+key+", value = "+value+"\n";
        }
        return result + " \n\n\n" + AlertConsumer.consumedMessages.size()
                +" messages have been consumed";
    }
}

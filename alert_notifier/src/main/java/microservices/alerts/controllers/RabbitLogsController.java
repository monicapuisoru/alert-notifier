package microservices.alerts.controllers;

import microservices.alerts.alertRepository.AlertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
public class RabbitLogsController {
    protected Logger logger = Logger.getLogger(RabbitLogsController.class
            .getName());
    protected AlertRepository alertRepository;

    @Autowired
    public RabbitLogsController(AlertRepository alertRepository) {
        this.alertRepository = alertRepository;
    }


    @RequestMapping("/logs")
    public String showLogs() {
        return "Hello from Logging Module!";
    }
}

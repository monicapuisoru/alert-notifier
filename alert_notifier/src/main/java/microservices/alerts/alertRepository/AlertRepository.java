package microservices.alerts.alertRepository;

import microservices.alerts.model.Alert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface AlertRepository  extends JpaRepository<Alert, Long> {
    @Query("select a from Alert a where a.alertId = ?1")
    List<Alert> findByAlertId(Long alertId);

   @Query("select a from Alert a")
    List<Alert> findAll();


    @Query("select count(*) from Alert")
    int countAlerts();

}

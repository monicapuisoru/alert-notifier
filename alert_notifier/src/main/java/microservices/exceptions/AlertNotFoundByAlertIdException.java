package microservices.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


    @ResponseStatus(HttpStatus.NOT_FOUND)
    public class AlertNotFoundByAlertIdException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        public AlertNotFoundByAlertIdException(String alertId) {
            super("No such alert: " + alertId);
        }
}

package microservices.services;

import microservices.services.consumer.AlertConsumerServer;
import microservices.services.registration.RegistrationServer;
import microservices.services.web.WebServer;

import java.net.InetAddress;


public class Main {

	public static final String NO_VALUE = "NO-VALUE";

	public static void main(String[] args) {

		String serverName = NO_VALUE;
		String port = null;

		System.setProperty(RegistrationServer.REGISTRATION_SERVER_HOSTNAME, "localhost");

		for (String arg : args) {
			if (arg.startsWith("--"))
				continue;

			if (serverName.equals(NO_VALUE))
				serverName = arg;
			else if (port == null)
				port = arg;
			else {
				System.out.println("Unexpected argument: " + arg);
				return;
			}
		}

		if (serverName == NO_VALUE) {
			return;
		}

		if (port != null)
			System.setProperty("server.port", port);

		try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			System.out.println("Running on IP: " + inetAddress.getHostAddress());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (serverName.equals("registration") || serverName.equals("reg")) {
			RegistrationServer.main(args);
		} else if (serverName.equals("alerts")) {
			AlertConsumerServer.main(args);
		} else if (serverName.equals("web")) {
			WebServer.main(args);
		} else {
			System.out.println("Unknown server type: " + serverName);
		}
	}
}
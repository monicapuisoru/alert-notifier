package microservices.services.rabbitLogs;

import microservices.alerts.AlertConfiguration;
import microservices.alerts.alertRepository.AlertRepository;
import microservices.services.registration.RegistrationServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

import java.util.logging.Logger;

@SpringBootApplication
@EnableDiscoveryClient
@Import(AlertConfiguration.class)
public class RabbitLogsServer {

    @Autowired
    protected AlertRepository alertRepository;

    protected Logger logger = Logger.getLogger(RabbitLogsServer.class.getName());

    public static void main(String[] args) {

        if (System.getProperty(RegistrationServer.REGISTRATION_SERVER_HOSTNAME) == null)
            System.setProperty(RegistrationServer.REGISTRATION_SERVER_HOSTNAME, "localhost");

        System.setProperty("spring.config.name", "rabbit-logs-server");

        SpringApplication.run(RabbitLogsServer.class, args);
    }
}

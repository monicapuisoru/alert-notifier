package microservices.services.web;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.math.BigDecimal;

@JsonRootName("Alert")
public class Alert{
    protected Long alertId;
    protected Long userId;
    protected Long marketId;
    protected Long coinId;
    protected BigDecimal threshold;
    protected BigDecimal currentAmount;

    public Long getAlertId() {
        return alertId;
    }

    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getMarketId() {
        return marketId;
    }

    public void setMarketId(Long marketId) {
        this.marketId = marketId;
    }

    public Long getCoinId() {
        return coinId;
    }

    public void setCoinId(Long coinId) {
        this.coinId = coinId;
    }

    public BigDecimal getThreshold() {
        return threshold;
    }

    public void setThreshold(BigDecimal threshold) {
        this.threshold = threshold;
    }

    public BigDecimal getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(BigDecimal currentAmount) {
        this.currentAmount = currentAmount;
    }


    @Override
    public String toString(){
        return "Alert id: " + alertId
                + ", userId: " + userId
                + ", marketId: " + marketId
                + ", coinId: " + coinId
                + ", currentAmount: " + currentAmount
                + ", threshold: " + threshold;
    }

}

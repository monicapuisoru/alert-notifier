package microservices.services.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class WebAlertService {

    @Autowired
    @LoadBalanced
    protected RestTemplate restTemplate;

    protected String serviceUrl;

    protected Logger logger = Logger.getLogger(WebAlertService.class.getName());

    public WebAlertService(String serviceUrl) {
        this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
    }

    @PostConstruct
    public void test() {
        logger.warning("The RestTemplate request factory is " + restTemplate.getRequestFactory().getClass());
    }


    public Alert findByAlertId(String alertId) {
        logger.info("findByAlertId() invoked: for " + alertId);
        Alert alert = null;
        try {
            alert = restTemplate.getForObject(serviceUrl + "/alerts/{alertId}", Alert.class, alertId);
        } catch (Exception e) {
            logger.severe(e.getClass() + ": " + e.getLocalizedMessage());
        }

        if (alert == null){
            logger.info("findByAlertId() Alert is null");
        } else {
            logger.info("findByAlertId() Alert found: alertId: "+ alert.getAlertId()+", userId: " + alert.getUserId()
                    +", marketId: "+alert.getMarketId()+", coinId: "+alert.getCoinId()+", amount: "
                    +alert.getCurrentAmount() +", threshold: "+alert.getThreshold());
        }

        return alert;
    }

    public List<Alert> findAll() {
        logger.info("findAll() invoked:");
        Alert[] alerts = null;

        try {
            alerts = restTemplate.getForObject(serviceUrl + "/alerts", Alert[].class);
        } catch (HttpClientErrorException e) {
           logger.warning("HttpClientErrorException");
        }

        if (alerts == null || alerts.length == 0) {
            logger.info("findAll() No alerts found");
            return null;
        }
        else {
            logger.info("findAll() Found "+alerts.length+" alerts");
            return Arrays.asList(alerts);
        }
    }

    public String countAlerts() {
        logger.info("countAlerts() invoked:");
        try {
            return restTemplate.getForObject(serviceUrl + "/count", String.class);
        } catch (Exception e) {
            logger.severe(e.getClass() + ": " + e.getLocalizedMessage());
            return "0";
        }
    }


}

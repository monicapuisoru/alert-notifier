package microservices.services.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.logging.Logger;

@Controller
public class WebAlertController {


    @Autowired
    protected WebAlertService alertService;

    protected Logger logger = Logger.getLogger(WebAlertController.class.getName());

    public WebAlertController(WebAlertService alertService) {
        this.alertService = alertService;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setAllowedFields("alertId", "count");
    }

    @RequestMapping("/index")
    public String home() {
        return "index";
    }

    @RequestMapping("/alerts")
    public List<Alert> alerts(Model model) {
        logger.info("web-server findAll() invoked: " );
        List<Alert>  alerts = alertService.findAll();

        if (alerts == null)
            logger.info("web-server findAll(): No alert found");
        else{
            logger.info("web-server findAll() found: " + alerts.size() + " alerts");
            model.addAttribute("alerts", alerts);
        }

        return alerts;
    }

    @RequestMapping("/alerts/{alertId}")
    public String byAlertId(Model model, @PathVariable("alertId") String alertId) {
        logger.info("web-server byAlertId() invoked: " + alertId);

        Alert alert = alertService.findByAlertId(alertId);
        if (alert == null) {
            logger.info("Alert is null");
        } else {
            logger.info("web-server byAlertId() found threshold: " + alert.getThreshold());
            model.addAttribute("alert", alert);
            model.addAttribute("alertId", alert.getAlertId());
            model.addAttribute("userId", alert.getUserId());
            model.addAttribute("coinId", alert.getCoinId());
            model.addAttribute("marketId", alert.getMarketId());
            model.addAttribute("currentAmount", alert.getCurrentAmount());
            model.addAttribute("threshold", alert.getThreshold());
        }

        return "alert";
    }

    @RequestMapping("/count")
    public String countAlerts(Model model) {
        logger.info("web-server countAlerts() invoked: ");
        String noOfAlerts = alertService.countAlerts();

        logger.info("web-server byAlertId() found: " + noOfAlerts + " alerts");
        model.addAttribute("count", noOfAlerts);

        return "count";
    }
}

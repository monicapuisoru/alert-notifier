insert into ALERTS (ALERT_ID, USER_ID, MARKET_ID, COIN_ID, CURRENT_AMOUNT, THRESHOLD) values ('1', '1', '1', '1', '100.00', '0.1');
insert into ALERTS (ALERT_ID, USER_ID, MARKET_ID, COIN_ID, CURRENT_AMOUNT, THRESHOLD) values ('2', '2', '2', '2', '200.00', '0.2');
insert into ALERTS (ALERT_ID, USER_ID, MARKET_ID, COIN_ID, CURRENT_AMOUNT, THRESHOLD) values ('3', '3', '3', '3', '300.00', '0.3');
insert into ALERTS (ALERT_ID, USER_ID, MARKET_ID, COIN_ID, CURRENT_AMOUNT, THRESHOLD) values ('4', '4', '4', '4', '400.00', '0.4');
insert into ALERTS (ALERT_ID, USER_ID, MARKET_ID, COIN_ID, CURRENT_AMOUNT, THRESHOLD) values ('5', '5', '5', '5', '500.00', '0.5');
insert into ALERTS (ALERT_ID, USER_ID, MARKET_ID, COIN_ID, CURRENT_AMOUNT, THRESHOLD) values ('6', '6', '6', '6', '600.00', '0.6');
insert into ALERTS (ALERT_ID, USER_ID, MARKET_ID, COIN_ID, CURRENT_AMOUNT, THRESHOLD) values ('7', '7', '7', '7', '700.00', '0.7');
insert into ALERTS (ALERT_ID, USER_ID, MARKET_ID, COIN_ID, CURRENT_AMOUNT, THRESHOLD) values ('8', '8', '8', '8', '800.00', '0.8');
insert into ALERTS (ALERT_ID, USER_ID, MARKET_ID, COIN_ID, CURRENT_AMOUNT, THRESHOLD) values ('9', '9', '9', '9', '900.00', '0.9');
insert into ALERTS (ALERT_ID, USER_ID, MARKET_ID, COIN_ID, CURRENT_AMOUNT, THRESHOLD) values ('10', '10', '10', '10', '1000.00', '0.95');
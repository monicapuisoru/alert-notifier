drop table ALERTS if exists;

create table ALERTS (
ALERT_ID bigint identity primary key,
USER_ID bigint,
MARKET_ID bigint,
COIN_ID bigint,
CURRENT_AMOUNT decimal(8,2),
THRESHOLD decimal(8,2)
);
